import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict, Counter
from itertools import permutations
totalVersion = 0
def solve(fileName):
	global totalVersion
	totalVersion = 0
	with open(fileName) as file:
		lines = [ line.strip() for line in file.readlines()]
	#print(lines)
	nums = []
	scale = 16
	for c in lines[0]:
		#print(int(c, scale))
		nums.append(bin(int(c, scale))[2:].zfill(4))

	#print(nums)
	n = "".join(nums)
	
	print(n)
	print(readPacket(n))
	return totalVersion



def readPacket(n):
	print("RP", n)
	global totalVersion
	v = int(n[:3],2)
	totalVersion += v
	t = int(n[3:6],2)
	print("n",n)
	rest = n[6:]
	print("r", rest)
	consumed = 6
	print("vt", v,t)
	# literal
	if t == 4:
		print("LIT",rest)
		num = ""
		while True:
			num += rest[1:5]
			consumed += 5
			if rest[0] == "0":
				break
			rest = rest[5:]
		return (v,t,int(num,2), consumed)
	else:
	# op a,b
		nestedPackets = []
		print("OP")
		I = rest[0]	
		print("I", I)
  
		print("rest", rest)
		lenI = 15 if I == "0" else 11
		L = rest[1:1+lenI]
		L = int(L,2)
		print("L", L)
		print("rest", rest)
		rest = rest[1+lenI:]
		consumed += 1+lenI
		print("rest", rest)
		vals = []
		if I == "0":
			rest = rest[:L]
			while rest:
				v = readPacket(rest)
				rest = rest[v[3]:]
				consumed+=v[3]
				vals.append(v[2])
		else:
			for i in range(L):
				print("i",i,"l",L)
				v = readPacket(rest)
				rest = rest[v[3]:]
				consumed+=v[3]
				vals.append(v[2])
		return (v,t,vals,consumed)

		pass
	exit(20)
		


if "__main__" == __name__:
	ans = 31
	ex = solve("in.txt")
	print(ex)
	print("ans", ans)
	if ex != ans:
		print("wrong")
		exit(1)
	print(solve("input.txt"))
	exit(0)