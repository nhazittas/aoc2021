import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict

def flash3(b,x,y):
	print("flash", x, y, b[x][y])
	for dx in range(-1,2):
		fx = x + dx
		if fx <0 or fx >= len(b):
			continue
		for dy in range(-1,2):
			fy = y + dy
			if fy < 0 or fy >= len(b[0] or (dx == 0 and dy == 0)):
				continue
			v = b[fx][fy]
			if type(v) is not int :
				continue
			b[fx][fy] = v+1
			print("+",fx,fy,v,v+1)
def flash(b):
	tf = 0
	while True:
		flashed = False
		for x in range(len(b)):
			for y in range(len(b[0])):
				v = b[x][y]
				if type(v) is int and v > 9:
					flash3(b,x,y)
					flashed = True
					b[x][y] = True
					tf +=1
		print(b)
		if not flashed:
			return ([[c if type(c) is int else 0 for c in cc] for cc in b], tf)
	exit(1)

def pp(b):
	return [[c+1 for c in cc] for cc in b]
def solve(fileName):
	with open(fileName) as file:
		lines = file.readlines()
		lines = [[int(c) for c in line.strip()] for line in lines]

	n = 10000
	b = lines
	tf = 0

	#for l in b:
		#print("".join(map(str,l)))
	for i in range(n):
		b, nf = flash(pp(b))
		tf += nf
		if nf == len(b)*len(b[0]):
			return i +1
	print(tf)
	for l in b:
		print("".join(map(str,l)))

	return tf


if "__main__" == __name__:
	ans = 195
	ex = solve("in.txt")
	print(ex)
	print(ans)
	if ex != ans:
		print("wrong")
		exit(1)
	print(solve("input.txt"))
	exit(0)