import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict

def calcMissing(wires):
	allWires = set("abcdefg")
	t = set(wires)
	re = [c for c in allWires.difference(t)]
	if len(re) == 1:
		return re[0]
	return re

def calcValue(a, b):
	numChars = [""]*10
	unknown = []
	for wires in a:
		wires = "".join(sorted(wires))
		l = len(wires)
		if l == 2:
			numChars[1] = wires
		elif l == 4:
			numChars[4] = wires
		elif l == 3:
			numChars[7] = wires
		elif l == 7:
			numChars[8] = wires
		else:
			#print("++",wires)
			unknown.append(wires)
	print("11111", numChars, unknown)
	stillDunno = []
	for u in unknown:
		l = len(u)
		if l == 6:
			# 0 6 9
			miss = calcMissing(u)
			if miss in numChars[1]:
				numChars[6] = u
			elif miss in numChars[4]:
				numChars[0] = u
			else:
				numChars[9] = u
		elif l == 5:
			# 2 3 5
			stillDunno.append(u)
	print("sd", stillDunno)
	print("sdnc", numChars)
	miss6 = calcMissing(numChars[6])
	miss9 = calcMissing(numChars[9])
	for sd in stillDunno:
		sdm = calcMissing(sd)
		print("33", sdm, miss6, miss9)
		# 2 3 5
		if miss6 in sdm:
			print(5)
			numChars[5] = sd
		else:
			if miss9 in sdm:
				print(3)
				numChars[3] = sd
			else:
				print(2)
				numChars[2] = sd
	print("solve:",numChars)
	bbb = []
	for bb in b:
		print("bb", bb)
		print("sb", "".join(sorted(bb)))
		bbb.append( "".join(sorted(bb)))
	re = ""

	print("nums:",bbb)
	for num in bbb:
		re += str(numChars.index(num))

	return int(re)
		

def solve(fileName):
	with open(fileName) as file:
		lines = file.readlines()
		lines = [line.strip() for line in lines]
  
	count = 0
 
	for line in lines:
		p1,p2 = line.split(" | ")
		#print(p1,p2)
		count += calcValue(p1.split(" "),p2.split(" "))
         
	return count

ans = 61229
ex = solve("in.txt")
print(ex)
print(ans)
if ex != ans:
	print("wrong")
	exit(1)
print(solve("input.txt"))