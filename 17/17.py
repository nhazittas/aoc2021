import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict, Counter
from itertools import permutations
totalVersion = 0

def inRange(v, range):
	return v>= range[0] and v<= range[1]

def sim(xRange,yRange,vx, vy):
	x,y = 0,0
	maxY = y
	#havent passed in x, havent dropped below in y
	while x <= xRange[1] and y > yRange[0]:
		x += vx
		y += vy
		maxY = max([y,maxY])
		if vx != 0:
			vx -= 1
		vy -= 1
		if inRange(x, xRange) and inRange(y, yRange):
			print("HIT",x,xRange, y, yRange)
			return (True, maxY)
		pass

	return (False, maxY)

def solve(fileName):
	global totalVersion
	totalVersion = 0
	with open(fileName) as file:
		lines = [ line.strip() for line in file.readlines()]
	#print(lines)
	pts = lines[0].split(": ")[1]
	print(pts)
	xRange,yRange = [ [ int(c) for c in p.split("=")[1].split("..")] for p in pts.split(", ")]
	maxHeight = 0
	hits = []
	for vx in range(2*xRange[1]):
		for vy in range(-xRange[1], 2*xRange[1]):
			hit, maxY = sim(xRange, yRange, vx, vy)
			if hit:
				maxHeight = max([maxHeight, maxY])
				hits.append((vx,vy))
	return len(hits)



if "__main__" == __name__:
	ans = 112
	ex = solve("in.txt")
	print(ex)
	print("ans", ans)
	if ex != ans:
		print("wrong")
		exit(1)
	print(solve("input.txt"))
	exit(0)