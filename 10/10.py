import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict

def checkPos(pos, board):
	print(pos)
	x,y = pos
	val = board[x][y]
	n = len(board)
	m = len(board[0])
	for i in range(-1,2):
		if 0<=x+i<n:
			for j in range(-1,2):
				if i== 0 and j==0:
					continue
				if 0<=y+j<m:
					if val >= board[x+i][y+j]:
						print("WRONG", i, j, val, board[x+i][y+j])
						return False
	print("YES", i, j)
	return True

             
					
                
def score(c):
	if c == ")":
		return 3 
	if c == "]":
		return 57
	if c == "}":
		return 1197 
	if c == ">":
		return 25137 
	return 0

def checkLine(line):
	print(line)
	stack = []
	starts = "<{[("
	ends = ">}])"
	for c in line:
		if c in starts:
			stack.append(c)
		elif c in ends:
			o = stack.pop()
			print(o)
			p = starts[ends.index(c)]
			print(o,p,c)

			if p != o:
				print("no match", c)
				return c
		else:
			print("ERROR", c)
			exit(1)
		print(c, "  ##  ", stack)
	return stack

def solve(fileName):
	with open(fileName) as file:
		lines = file.readlines()
		lines = [line.strip() for line in lines]

	cla = [checkLine(line) for line in lines]
	print(cla)
	print(cla)

	return sum([score(cl) for cl in cla])


ans = 26397
ex = solve("in.txt")
print(ex)
print(ans)
if ex != ans:
	print("wrong")
	exit(1)
print(solve("input.txt"))