import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict

def opp(val, foldValue):
	dist = abs(foldValue - val)
	re = foldValue - dist
	print("Opp", val, foldValue, re)
	return re

def reflect(p, f):
	x,y = p
	axis, val = f
	print("R", x, y, axis, val, axis=="y")
	if axis == "y":
		return (x,opp(y, val))
	else: #assume x
		return (opp(x, val),y)

def fold(points, f):
	retVal = set()
	for p in points:
		newP = reflect(p,f)
		retVal.add(newP)
	return retVal

def printP(points,maxX, maxY):
        
	print(maxX, maxY)
	# print()
	# xs = [p[0] for p in points]
	# minX = min(xs)
	# maxX = max(xs)
	# ys = [p[1] for p in points]
	# minY = min(ys)
	# maxY = max(ys)
	for y in range(0, maxY+1):
		for x in range(0, maxX +1):
			print("#" if (x,y) in points else ".", end="")
		print()

	print()


def solve(fileName):
	points = set()
	folds = []
	with open(fileName) as file:
		seenEmpty = False
		for line in file.readlines():
			line = line.strip()
			if not seenEmpty and len(line) == 0:
				seenEmpty = True
				continue
			if seenEmpty:
				#folds
				eq = line.split()[-1]
				xORy, val = eq.split("=")
				val = int(val)
				folds.append((xORy, val))

			else:
				#points
				x, y = [int(c) for c in line.split(",")]
				points.add((x,y))
	print(points)
	print(folds)
	xs = [p[0] for p in points]
	minX = min(xs)
	maxX = max(xs)
	ys = [p[1] for p in points]
	minY = min(ys)
	maxY = max(ys)

	for f in folds:
        
		printP(points, maxX, maxY)
		points = fold(points,f)
		if f[0] == "y":
			maxY = f[1] - 1
		else:	
			maxX = f[1] - 1

	printP(points, maxX, maxY)
	print(maxX, maxY, (maxX+1)*(maxY+1), len(points))

	return 0


if "__main__" == __name__:
	ans = 0
	ex = solve("in.txt")
	print(ex)
	print("ans", ans)
	if ex != ans:
		print("wrong")
		exit(1)
	print(solve("input.txt"))
	exit(0)