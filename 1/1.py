import sys
import re
from functools import lru_cache

valid = 0

lines = [int(line.strip()) for line in sys.stdin]

a = b = c = 0

for i in range(len(lines)-1):
	a = lines[i]
	b = lines[i+1]
	if a < b:
		c+=1

print(c)
