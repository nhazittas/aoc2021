import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict

def solve(fileName):
	with open(fileName) as file:
		lines = file.readlines()
		lines = [line.strip() for line in lines]

		edges = defaultdict(list)
		for edge in lines:
			#print("edge",edge)
			start, stop = edge.split("-")
			edges[start].append(stop)
			edges[stop].append(start)

		#print(edges)
		start = "start"
		sts =[f'{start},{e}' for e in edges[start]]
		partialPaths = set([(st, "") for st in sts])
		completePaths = set()
		partialPathHasSSCTset = set()
		#print("PP", partialPaths)
		while len(partialPaths) > 0:
			npp = set()
			for partialPath, repeater in partialPaths:
				lastDot = partialPath.rfind(",")
				#print(">",partialPath, repeater)
				lastWord = partialPath[lastDot+1:] if lastDot >= 0 else partialPath
				for e in edges[lastWord]:
					np = f'{partialPath},{e}'
					#print(np)
					if e == "end":
						completePaths.add(np)
					elif e == "start":
						continue
					else:
						#print("e", partialPath, e )
						c = partialPath.count(e)
						#print(c)
						partialPathHasSSCT = len(repeater) > 0
						partialPathHasSSCTSamLettr = partialPathHasSSCT and c == e
						allowedCount = 300 if e.isupper() else ( 1 if partialPathHasSSCT else 2) 
						#print("ac", allowedCount, "r", repeater)
						myRepeater = repeater
						if repeater == "" and c == 1 and not e.isupper():
							#print("repp", e, partialPath, c)
							myRepeater = e
						if c + 1 <= allowedCount:
							#print("+", partialPath,e )
							npp.add((np, myRepeater))
						else:
							pass
							#print("by by", partialPath, e)
							
				#print("partialPath", npp)
				#print("completePaths", completePaths)
				#print("sts", sts)
				#print("lw", lastWord)
			partialPaths = npp
		#print(completePaths, len(completePaths))
		for path in sorted(completePaths):
			print(path)
	return len(completePaths)


if "__main__" == __name__:
	ans = 36
	ex = solve("in.txt")
	print(ex)
	print("ans", ans)
	if ex != ans:
		print("wrong")
		exit(1)
	print(solve("input.txt"))
	exit(0)