import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict

def triNum(n):
        return n*(n+1)/2

def solve(fileName):
	with open(fileName) as file:
		lines = file.readlines()
		lines = [line.rstrip() for line in lines]
		line = [int(c) for c in lines[0].split(",")]
  
	print(line)
	minGas = 99999**9999
	minI = -1
	for i in range(min(line), max(line)):
		cost = sum([ triNum(abs((i-c))) for c in line])
		if cost < minGas:
			minGas = cost
			minI = i
		minGas = min(cost, minGas)
         
	return minGas

ans = 168
ex = solve("in.txt")
print(ex)
print(ans)
if ex != ans:
	print("wrong")
	exit(1)
print(solve("input.txt"))