import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict

valid = 0

lines = [ line.strip() for line in sys.stdin]

# 565,190 -> 756,381
upnDowns = []
board = defaultdict(lambda: 0)
for line in lines:
	points = [ [int(c) for c in pair.split(",")] for pair in line.split(" -> ")]	
	print(points)
	p1 = points[0]		
	x1, y1 = p1
	p2 = points[1]		
	x2, y2 = p2
	print(x1,y1,x2,y2)
	if x1 == x2:
		print("vert")
		upnDowns.append((p1,p2))
		for y in range(min(y1,y2), max(y1,y2) +1):
			board[(x1,y)] +=1
	elif y1 == y2:
		print("horz")
		upnDowns.append((p1,p2))
		for x in range(min(x1,x2), max(x1,x2) +1):
			board[(x,y1)] +=1
	else:
		print("DIAG")
		dx = x2 - x1
		dx /= (abs(dx))
		dx = int(dx)
		dy = y2 - y1
		dy /= (abs(dy))
		dy = int(dy)
		print(dy,dx)
		
		
		cx = x1
		cy = y1
		board[(cx, cy)] +=1
		while cx != x2:
			cx += dx
			cy += dy
			print(cx,cy)
			board[(cx, cy)] +=1

print(board)
score = 0
for key, value in board.items():
	if value > 1:
		score +=1

print(score)


	
	
