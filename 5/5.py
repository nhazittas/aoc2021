import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict

valid = 0

lines = [ line.strip() for line in sys.stdin]

# 565,190 -> 756,381
upnDowns = []
board = defaultdict(lambda: 0)
for line in lines:
	points = [ [int(c) for c in pair.split(",")] for pair in line.split(" -> ")]	
	print(points)
	p1 = points[0]		
	x1, y1 = p1
	p2 = points[1]		
	x2, y2 = p2
	print(x1,y1,x2,y2)
	if x1 == x2:
		upnDowns.append((p1,p2))
		for y in range(min(y1,y2), max(y1,y2) +1):
			board[(x1,y)] +=1
	if y1 == y2:
		upnDowns.append((p1,p2))
		for x in range(min(x1,x2), max(x1,x2) +1):
			board[(x,y1)] +=1
		

print(board)
score = 0
for key, value in board.items():
	if value > 1:
		score +=1

print(score)


	
	
