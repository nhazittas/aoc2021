import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict, Counter
from itertools import permutations

def getFrom(board, x, y):
	n = len(board)
	m = len(board[0])
	qx, rx = divmod(x, n)
	qy, ry = divmod(y, m)
	v = (board[rx][ry] + qx + qy)
	while v > 9:
		v -= 9
	return v


def h(x,y, n, m, board):
	return getFrom(board,x,y)

def getNeighbors(x,y,n,m):
	retVal = []
	if 0<x :
		retVal.append( (x-1, y))
	if 0<y :
		retVal.append( (x, y-1))
	if x < n*5-1 :
		retVal.append(( x+1, y))
	if y < m*5-1 :
		retVal.append((x, y+1))
	#print(retVal)
	return retVal

def findChepest(board):

	n = len(board)
	m = len(board[0])
	goal = (n*5-1,m*5-1)
	hScore = lambda x,y: h(x,y,n,m, board)
	getN = lambda x,y: getNeighbors(x,y,n,m)
	
	openSet = set([(0,0)])
	cameFrom = {}

	gScore = defaultdict(lambda: 2**59)
	gScore[(0,0)] = 0
 

	fScore = defaultdict(lambda x: 2**59)
	fScore[(0,0)] = hScore(0,0)


	while len(openSet) > 0:
		current = sorted(openSet, key = lambda x: fScore[(x[0],x[1])])[0]
		#print("current", current)
		retVal = gScore[current]
		if current == goal:
			path = [current]
			while current in cameFrom:
				current = cameFrom[current]
				path.insert(0,current)
			#print(path)
			#print("RETURN", gScore)
			#print("RETURN", gScore[current])
			return retVal
		x,y = current
		openSet.remove(current)
		for neighbor in getN(x,y):
			nx,ny = neighbor
				
			#print("n", neighbor)
			thisGscore = gScore[current] + getFrom(board,nx,ny)
			#print("tg", thisGscore)
			#print("gs", gScore[neighbor])
			if nx <0 or ny<0 or nx == 5*n or ny ==5*m:
				print("HALT YOU HAVE VOILATED THE LAW")
				exit(2)
			if thisGscore < gScore[neighbor]:
				gScore[neighbor] = thisGscore
				cameFrom[neighbor] = current
				fScore[neighbor] = thisGscore + hScore(x,y)
				openSet.add(neighbor)



	
	return 2**60

        


def solve(fileName):
	with open(fileName) as file:
		lines = [[int(c) for c in line.strip()] for line in file.readlines()]
	#print(lines)
	n = len(lines)
	m = len(lines[0])
	
	print(n, m)
	return findChepest(lines)



if "__main__" == __name__:
	ans = 315
	ex = solve("in.txt")
	print(ex)
	print("ans", ans)
	if ex != ans:
		print("wrong")
		exit(1)
	print(solve("input.txt"))
	exit(0)