import sys
import re
from functools import lru_cache

valid = 0

lines = [line.strip().split() for line in sys.stdin]
print(lines)

h = d = a = 0

for line in lines:
	distance = int(line[1])
	dir = line[0]
	if dir == "forward":
		h += distance
	elif dir == "down":
		d += distance
	elif dir == "up":
		d -= distance
	print(d, h, a)

print(h*d)	
