import sys
import re
from functools import lru_cache

valid = 0

lines = [line.strip().split() for line in sys.stdin]
print(lines)

h = d = a = 0

for line in lines:
	distance = int(line[1])
	dir = line[0]
	if dir == "forward":
		h += distance
		d += a*distance
	elif dir == "down":
		a += distance
	elif dir == "up":
		a -= distance
	print(d, h, a)

print(h*d)	
