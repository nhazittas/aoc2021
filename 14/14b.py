import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict, Counter

def solve(fileName):
	with open(fileName) as file:
		lines = [line.strip() for line in file.readlines()]
	#print(lines)
	og = lines[0]
	rules = {}

	pairCount = defaultdict(int)
	for i in range(len(og)-1):
		a = og[i]
		b = og[i+1]
		pairCount[a+b] += 1

	for line in lines[2:]:
		ab, c = line.split(" -> ")
		a = ab[0]
		b = ab[1]
		#print(a,b,c)
		rules[(a,b)] = c
	#print(rules)
	poly = og
	for i in range(40):
		print("i", i)
		#poly = nextP(poly, rules)
		#print(poly)
		nextPairCount = defaultdict(int)
		for pair in pairCount:
			freq = pairCount[pair]
			a = pair[0]
			b = pair[1]
			c = rules[(a,b)]
			nextPairCount[a+c] += freq
			nextPairCount[c+b] += freq
		pairCount = nextPairCount

	print(pairCount)
	aFreq = defaultdict(int)
	bFreq = defaultdict(int)
	for pair in pairCount:
		freq = pairCount[pair]
		a = pair[0]
		b = pair[1]
		aFreq[a] += freq
		bFreq[b] += freq
	letters = set(aFreq.keys()) | set(bFreq.keys())
	print("letters", letters)
	trueFreq = defaultdict(int)
	for letter in letters:
		trueFreq[letter] = max([aFreq[letter], bFreq[letter]])

	v = trueFreq.values()
	print(max(v), min(v),max(v)- min(v) )
	return max(v) - min(v)

def nextP(p, rules):
	newP = p[0]
	for i in range(len(p)-1):
		a = p[i]
		b = p[i+1]
		i = rules[(a,b)]
		#print("i", i)
		if len(i) > 0:
			newP += i
		else:
			#print("no i", i)
			pass
		newP += b
	return newP



if "__main__" == __name__:
	ans = 2188189693529
	ex = solve("in.txt")
	print(ex)
	print("ans", ans)
	if ex != ans:
		print("wrong")
		exit(1)
	print(solve("input.txt"))
	exit(0)