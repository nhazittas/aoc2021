import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict, Counter

def solve(fileName):
	with open(fileName) as file:
		lines = [line.strip() for line in file.readlines()]
	#print(lines)
	og = lines[0]
	rules = {}

	for line in lines[2:]:
		ab, c = line.split(" -> ")
		a = ab[0]
		b = ab[1]
		#print(a,b,c)
		rules[(a,b)] = c
	#print(rules)
	poly = og
	for i in range(40):
		print("i", i)
		poly = nextP(poly, rules)
		#print(poly)
	freq = defaultdict(int)
	for c in poly:
		freq[c] += 1
	v = freq.values()
	print(max(v), min(v),max(v)- min(v) )
	return max(v) - min(v)

def nextP(p, rules):
	newP = p[0]
	for i in range(len(p)-1):
		a = p[i]
		b = p[i+1]
		i = rules[(a,b)]
		#print("i", i)
		if len(i) > 0:
			newP += i
		else:
			#print("no i", i)
			pass
		newP += b
	return newP



if "__main__" == __name__:
	ans = 2188189693529
	ex = solve("in.txt")
	print(ex)
	print("ans", ans)
	if ex != ans:
		print("wrong")
		exit(1)
	print(solve("input.txt"))
	exit(0)