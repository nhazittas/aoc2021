import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict

def checkPos(pos, board):
	print(pos)
	x,y = pos
	val = board[x][y]
	n = len(board)
	m = len(board[0])
	for i in range(-1,2):
		if 0<=x+i<n:
			for j in range(-1,2):
				if i== 0 and j==0:
					continue
				if 0<=y+j<m:
					if val >= board[x+i][y+j]:
						print("WRONG", i, j, val, board[x+i][y+j])
						return False
	print("YES", i, j)
	return True
             
					
                

def solve(fileName):
	with open(fileName) as file:
		lines = file.readlines()
		lines = [[int(c) for c in line.strip()] for line in lines]
	print(lines)
	n = len(lines)
	m = len(lines[0])
	positions = set([(i,j) for j in range(m) for i in range(n) if checkPos((i,j), lines)])
	print(positions)
	return sum(map(lambda x: lines[x[0]][x[1]]+1, positions))

ans = 15
ex = solve("in.txt")
print(ex)
print(ans)
if ex != ans:
	print("wrong")
	exit(1)
print(solve("input.txt"))