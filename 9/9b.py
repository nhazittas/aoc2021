import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict

def checkPos(pos, board):
	print(pos)
	x,y = pos
	val = board[x][y]
	n = len(board)
	m = len(board[0])
	for i in range(-1,2):
		if 0<=x+i<n:
			for j in range(-1,2):
				if i== 0 and j==0:
					continue
				if 0<=y+j<m:
					if val >= board[x+i][y+j]:
						print("WRONG", i, j, val, board[x+i][y+j])
						return False
	print("YES", i, j)
	return True
             
def nexBasin(basin, board):
	nb = set()					
	for pos in basin:
		x,y = pos
		val = board[x][y]
		n = len(board)
		m = len(board[0])
		for i in range(-1,2):
			if 0<=x+i<n:
				for j in range(-1,2):
					if i== 0 and j==0 or abs(i)+abs(j) > 1:
						continue
					if 0<=y+j<m:
						if val < board[x+i][y+j] and board[x+i][y+j] < 9:
							nb.add((x+i, y+j))
							print((x+i, y+j))
	print([c for c in nb])
	return nb

def calcBasinSize(basin, board):
	s = 0
	allBasin = set(basin)
	print(basin)
	for i in range(11):
		s += len(basin)
		basin = nexBasin(basin, board)
		for b in basin:
			allBasin.add(b)
	print(s, len(allBasin))
	return len(allBasin)
         
		
                


def solve(fileName):
	with open(fileName) as file:
		lines = file.readlines()
		lines = [[int(c) for c in line.strip()] for line in lines]
	print(lines)
	n = len(lines)
	m = len(lines[0])
	positions = set([(i,j) for j in range(m) for i in range(n) if checkPos((i,j), lines)])
	print(positions)
	basins = [set([pos]) for pos in positions]
	print(basins)
	bs = sorted(map(lambda x: calcBasinSize(x, lines), basins), reverse=True)
	print(bs[0:3])
	return reduce(lambda x,y: x*y, bs[0:3])

ans = 1134
ex = solve("in.txt")
print(ex)
print(ans)
if ex != ans:
	print("wrong")
	exit(1)
print(solve("input.txt"))