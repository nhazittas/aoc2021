import sys
import re
from functools import lru_cache, reduce

from collections import defaultdict

valid = 0

lines = [ line.strip() for line in sys.stdin]
nums = [int(c) for c in lines[0].split(",")]
print(nums)

fishes = defaultdict(lambda:0)

for num in nums:
        fishes[num] += 1

print(fishes)

day = 0

while day < 256:
	nF = defaultdict(lambda:0)
	for daysOld in fishes:
		numF = fishes[daysOld]
		print(daysOld, numF)
		if daysOld == 0:
			nF[6] += numF
			nF[8] += numF
		else:
			nF[daysOld-1] += numF
	day +=1
	fishes = nF
	print(day, fishes)
print(sum(fishes.values()))