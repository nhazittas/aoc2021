import sys
import re
from functools import lru_cache, reduce

valid = 0

lines = [ line.strip() for line in sys.stdin]

numbers = [int(n) for n in lines[0].split(",")]

print(numbers)

def convertToChecked(board, pn):
	return [[n in pn for n in row] for row in board]

def checkBools(board):
	#Check rows
	for row in board:
		if reduce(lambda x,y: x and y, row):
			print("row winner:",row)
			return True
	#Check cols
	cols=[]
	diags=[]
	diag1 = []
	diag2 = []
	rl = len(board)
	for x in range(len(board)):
		cols.append([row[x] for row in board ])
		diag1.append(board[x][x])
		diag2.append(board[x][rl-1-x])
	diags.append(diag1)
	diags.append(diag2)
	for col in cols:
		if reduce(lambda x,y: x and y, col):
			print("col winner:",col)
			return True
#	#Check diags
#	for col in diags:
#		if reduce(lambda x,y: x and y, col):
#			print("diag winner:",col)
#			return True
	return False

def score(board, buuls):
	count = 0
	for i in range(len(board)):
		for j in range(len(board[0])):
			print(board[i][j], end="")
			if not buuls[i][j]:
				count += board[i][j]
				print("+")
			else:
				print("-")
	return count

boards = []
board = []
for i in range(2,len(lines)):
	line = lines[i]
	if len(line) == 0:
		boards.append(board)
		board = []
		print(0)
		continue
	else:
		print(line.split(" "))
		numLine = [int(num) for num in line.split(" ") if len(num)>0]
		board.append(numLine)
print(boards)

pickedNumbers = set()
while True:
	picked = numbers.pop(0)
	print("and the number is:", picked)
	pickedNumbers.add(picked)
	print("pn", pickedNumbers)
	nxtBoards=[]
	for board in boards:
		buuls=convertToChecked(board, pickedNumbers)
		print(buuls)
		if checkBools(buuls):
			print("boardDown!")
			if len(boards) == 1:
				print("loser!")
				print(board)
				sc = score(board, buuls)
				print(sc)
				print(sc*picked)
				exit(1)
		else:
			nxtBoards.append(board)
	boards = nxtBoards
picked = numbers.pop(0)
pickedNumbers.add(picked)
print(boards[0])
sc = score(boards[0], buuls)
print(sc)
print(picked)
print(sc*picked)

	



exit(0)
